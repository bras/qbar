#ifndef Room_h
#define Room_h

#include "Arduino.h"
#include "definitions.h"

class Room
{
  public:
	Room();
	void update();
	void prepare();
	void setBoxState(int id, int state);
	int getSleepersInRoom();
  private:
	int boxes[NUMBEROFBOXES];

  };
#endif

