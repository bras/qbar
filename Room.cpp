#include "Arduino.h"
#include "Room.h"
#include "definitions.h"


Room::Room(){

}

void Room::prepare(){
//Fill it with sleepstates

boxes[0] = SLEEPSTATE;
boxes[1] = SLEEPSTATE;
boxes[2] = SLEEPSTATE;
boxes[3] = SLEEPSTATE;
boxes[4] = SLEEPSTATE;
boxes[5] = SLEEPSTATE;
boxes[6] = SLEEPSTATE;

}

void Room::update(){


}

void Room::setBoxState(int id, int state){

boxes[id] = state;

}

int Room::getSleepersInRoom(){
int sleepers = 0;
for(int i = 0; i < NUMBEROFBOXES; i++){
if(boxes[i] == SLEEPSTATE){
sleepers++;
}
}
return sleepers;
}
