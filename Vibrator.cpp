#include "Arduino.h"
#include "Vibrator.h"
#include "definitions.h"

Vibrator::Vibrator(){
	_vibrationPin = VIBRATOR_PWM_PIN;
}

void Vibrator::prepare(){
	pinMode(_vibrationPin, OUTPUT);
}

void Vibrator::vibrate(int intensity){
	analogWrite(_vibrationPin, intensity); 
}


