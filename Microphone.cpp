#include "Arduino.h"
#include "Microphone.h"
#include "definitions.h"


Microphone::Microphone(){
}

Microphone::Microphone(MotionSensor* mSensor){
	_microphonePin = MICROPHONE_ANALOG_IN_PIN;
	_mSensor = mSensor;
}

void Microphone::prepare(){
	pinMode(_microphonePin, INPUT);
	_deciMax  = 0;
	resetClaps();
}

void Microphone::resetClaps() {
	_claps[0] = 0;
	_claps[1] = 0;
	_claps[2] = 0;
	_claps[3] = 0;
	_claps[4] = 0;
	_claps[5] = 0;
	_claps[6] = 0;
	_claps[7] = 0;
	_claps[8] = 0;
	_claps[9] = 0;
	_numClaps = 0;
}

void Microphone::printRawData() {
  int amp = map(analogRead(_microphonePin), 0, 1024, 0, 40);
//  Serial.print("Micdata: ");
  for (int i=0; i<amp; i++) {
//		Serial.print("|");
	}
//	Serial.println(" ");
}

int Microphone::getLastAmp() {
	return _lastAmp;
}

int Microphone::getNumClaps() {
	return _numClaps;
}

int Microphone::getTimeSinceLastClap() {
	return millis() - _lastClap;
}

boolean Microphone::checkClaps(int pattern[]) {
	int pMax = 0;
	int cMax = 0;
	int pNum = 0;
	int cNum = 0;
	for (int i=0; i<10; i++) {
		if (pattern[i]      > pMax) { pMax = pattern[i]; }
		if (_claps[i] > cMax) { cMax = _claps[i]; }
		if (pattern[i]      != 0) { pNum++; }
		if (_claps[i] != 0) { cNum++; }
	}
	
	if (pNum != cNum) { return false; }
	
	for (int i=0; i<10; i++) {
		int pDelay = map(pattern[i],      0, pMax, 0, 100);
		int cDelay = map(_claps[i], 0, cMax, 0, 100);
		
		if (cDelay - CLAP_ACCURACY > pDelay && cDelay + CLAP_ACCURACY < pDelay) { return false; }
	}
	
	return true;
}

void Microphone::update() {
  int amp = abs(map(analogRead(_microphonePin), 0, 1023, -512, 512));
  
  _ampAvg += amp;
  _numAvg++;
  
 	if (millis() - _lastAverage < 1) { return; }
  
  _lastAmp = _ampAvg / _numAvg;
	_ampAvg = _numAvg = 0;

  _deciMax = _lastAmp > _deciMax ? _lastAmp : _deciMax;
  
  // Timingspesifikke ting
  
  if (millis() - _lastClap > CLAP_MAX_SILENCE && _numClaps > 0) { // Resette klappingen om det har gått for lang tid
  	resetClaps();
  } 
  
  if (!_clapStarted && millis() - _lastClap > CLAP_MIN_DELAY && _lastAmp > CLAP_THRESHOLD && _numClaps < 11 && _mSensor->getMaxSecond() < 3) { // Sjekke for klapping
  	_clapStarted   = true;
  	_clapStartTime = millis();
  	//Serial.print("Started clap: ");
  	//Serial.println(_lastAmp);
  }
  
  if (_clapStarted && millis() - _clapStartTime > CLAP_MAX_DURATION) {
  	_clapStarted = false;
  	_lastClap    = false;
  	//Serial.println("Time out for clap");  
  }
  
  if (_clapStarted && millis() - _clapStartTime > CLAP_MIN_DURATION && _lastAmp < CLAP_THRESHOLD && _mSensor->getMaxSecond() < 3) {
  	//Serial.print("Klapp! ");
  	//Serial.println(millis() - _clapStartTime);
  	//Serial.print("Has not moved: ");
  	//Serial.println(_mSensor->getMaxSecond());
   	if (_numClaps == 0) {
 			_lastClap = millis();
			_numClaps++;
 		}
	 	else {
	  	int delay = millis() - _lastClap;
	  	_claps[_numClaps] = delay;
	  	_lastClap = millis();
 			_numClaps++;
 		 }
 		 _clapStarted = false;
  }
  
  if (millis() % 100 == 0) { // Gammel mønsterkode, kan tas bort
	  _deciMax = 0;
  } 
}