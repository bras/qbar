
#ifndef Xbee_h
#define Xbee_h

#include "Arduino.h"
#include "Room.h"

class NetworkListener
{
  public:
    inline virtual void stateChanged(char qbId, char stateId, int value) {};
    inline virtual void commandReceived(int command) {}; 
    inline virtual char getQbId() {};
};

class Xbee
{
  public:
    Xbee();
	  Xbee(NetworkListener* listener);
	  
	  void prepare();    
	  void prepareDebug();
    void reset(int delayTime);
    void resetNetworking();
    void broadcastState(int state, int value);
	  void broadcastCommand(char id, char data_1, char data_2);
	  void broadcastCommand(char id, int data);
    Room getRoom();
    char getQbId();
    void setQbId(char id);
	  String getXbeeAdress();
	  void update();
	String  getXbeeModemATSL();
  private:
	  NetworkListener* _listener;
	  
	  void    updateRoom();
    void    updateSerial();
    void    ping();
    int     getCent(char* data);
  	char    _qbId;
  	void    processResetCommand(char* data);
  	void    processNetworkCommands(char* data);
  	void    processStateCommands(char* data);
  	void    processDebugCommands(char* data);
	  void    processBroadCastCommands(char* data);
  	void    listenForReset();    
    void    resetForUpload();
    boolean returnedOK(); 
  	String  _myAdress;
  	int     _resetPin;
  	void    joinResetNetwork();
  	void    joinQbNetwork();
    boolean properMessageStart();
    void    dispatchCommand(char command, char* data);
};

#endif


/*
ein xbee har e
me kjører i AT mode ftw

Network
We are using two networks, a network for interactivity, and a resetnetwork
PAN id 1337 = Qbnetwork
PAN id 1939 = Resetnetwork

NR: 01 - 0013A200 407696B2 

NR: 19 - 0013A200 406FCAAB (BLAA) (ROUTER) Chip based antennaz0r
NR: 20 - 0013A200 406FC95D (RAUD) (COORDINATOR) Chip based anntennaz0r 
NR: 21 - 0013A200 40764768 (Blaa) COORDINATOR 1337 nettverket 					ATID1337,DH0,DLFFFF,BD7,RO10,WR,CN
NR: 22 - 0013A200 407A393F (raud) COORDINATOR 1939 nettverket (dvs resetnoden, sett denne i joinResetNetwork()) 	ATID1939,DH0013A200,DL407696B2,BD7,RO10,WR,CN


The resetnetwork has a designated xbee with an adapter as parent, should run with the config
DL-> destinasjonen til den som du vil linke opp med, denne vil endre seg, frå vår side
BD7 = 115200 baud //funker //TO upload, hit reset as the compile completes.

BD6 = 57600 baud
RO10 = Packet timeout set høg
Dersom det går haywire, reset til 9600 via ctu, det kan hande at dersom de allereie er linket saman, og ein prøver AT cmd, så blir de sendt over heile satan.
Mulig løysing, setje av 18 panID adresser til resets. evt bruke serial.write, framfor serial.print.

//Should probably reset these mofos to default before setting so much config?
AT COORDNATOR AT firmware -> and AT CONFIG ->	ATID1939,DH0013A200,DL406FCAAB,BD7,RO10,WR,CN
C:\Program Files\arduino-0022\hardware\tools\avr\bin\
avrdude.exe -c stk500v1 -b 57600 -P COM4 -p m328p -u -U flash:w:C:\Users\ParaMac\Documents\Arduino\Blink\Blink.cpp.hex

C:\Users\ParaMac\Documents\Arduino\Blink\Blink.cpp.hex

Any resetable arduino should drop into this one to one network on reset
Den skal alltid være dette, siden denne alltid skal peke til coordinator
AT ROUTER AT firmware -> and AT CONGIFIg -> ATID1939,DH0013A200,DL406FC95D,BD7,RO10,WR,CN
paste of click returnz0r in coolterm

//Teststiff
ATMY1234,DL5678,DH0,ID0,WR
ATMY5678,DL1234,DH0,ID0,WR

//Packet timeout (RO10 might have something to do with the fact that it takes a while to reset?) long timeouts for the packets


TO upload, hit reset as the compile completes.
*reset 

*/
