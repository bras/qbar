#include "Arduino.h"
#include "effects.h"

/**
 * Effects
 */

 
BlinkEffect::BlinkEffect() 
{

}  
 
BlinkEffect::BlinkEffect(QB* qb) 
{
  init(qb, 1000);
}  
 
BlinkEffect::BlinkEffect(QB* qb, int time) 
{
  init(qb, time);
}  

void BlinkEffect::init(QB* qb, int time) 
{
  _qb = qb;
  _time = time;
  
}  

void BlinkEffect::reset() {
  _qb->setOn(false);
}

void BlinkEffect::startEffect()
{
}

void BlinkEffect::updateEffect()
{
  QB* qb = _qb;
  
  if (millis() - _lastBlink > _time) 
  {
    boolean on = !qb->isOn();
    qb->setOn(on);
    _lastBlink = millis();

  }
}


void BlinkEffect::stopEffect()
{
}

int BlinkEffect::getLastBlink()
{
  return _lastBlink;
}


//
//
// Pulsateeffectz0r
//
//

PulsateEffect::PulsateEffect() 
{
  
}  

PulsateEffect::PulsateEffect(QB* qb) 
{
  init(qb, 0, 255, 1000);
}  

PulsateEffect::PulsateEffect(QB* qb, int from, int to)
{
  init(qb, from, to, 1000);
}

PulsateEffect::PulsateEffect(QB* qb, int from, int to, int cycle)
{
  init(qb, from, to, cycle);
}

void PulsateEffect::init(QB* qb, int from, int to, int cycle)
{
  _qb         = qb;
  _change     = 1;
  _lastChange = 0;
  _from       = constrain(from, 0, 255);
  _to         = constrain(to, 0, 255);
  _cycle      = cycle;
  _brightness = _from;
}

void PulsateEffect::reset() {
  _brightness = _from;
}

void PulsateEffect::startEffect()
{
	reset();
  _qb->setOn(true); // Må slås på i tilfelle andre states har slått den av
}

void PulsateEffect::updateEffect()
{

	if (millis() - _lastChange < _cycle / (_to - _from)) { return; }

	_brightness =  constrain(_brightness + _change, _from, _to);
  
  if (_brightness == _from ||  _brightness == _to) {
    _change = -_change ; 
  }     
  
  int motion = 0; //map(constrain(_qb->getMotionSensor()->getDelta(), 8, 24), 8, 24, 0, 255);
  
  _qb->setValue(_brightness + motion);  
  
  _lastChange = millis();
}

void PulsateEffect::stopEffect()
{
//Todo
}

TripEffect::TripEffect() 
{

}  
 
TripEffect::TripEffect(QB* qb) 
{
  init(qb);
}  

void TripEffect::init(QB* qb) 
{
  _qb = qb; 
}  

void TripEffect::startEffect()
{
}

void TripEffect::updateEffect()
{
  int motion = map(_qb->getMotionSensor()->getDelta(), 5, 128, 0, 255);
  
  _qb->setValue(motion);
  
}


void TripEffect::stopEffect()
{
}

//
//
// Vibrateeffect
//
//

VibrateEffect::VibrateEffect()
{

}

void VibrateEffect::init()
{

}

void VibrateEffect::updateEffect()
{

}

