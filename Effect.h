#ifndef Effect_h
#define Effect_h

#include "Arduino.h"


class Effect
{
  public:
    Effect(); 
    inline virtual void startEffect()  {};
    inline virtual void updateEffect() {};
    inline virtual void stopEffect()   {};
    inline virtual void reset()        {};
};
#endif
