#ifndef State_h
#define State_h

#include "Arduino.h"
#include "Transition.h"
#include "QB.h"

class Transition;

class State
{
  public:
    State(QB* qb); 
    inline virtual void enter() { enter(0);};
    inline virtual void enter(int value) { initTransitions(); _qb->enteredState(getStateId(), value); };
    inline virtual State* execute() { return checkTransitions(); }; // metode for å utføre de handlinger som denne tilstanden tilsier (i.e. svak fading) - må implementeres i subklasse
    inline virtual void exit() {};
    inline virtual char getStateId() { return '*'; };
    void addTransition(Transition* t, State* to);
    void initTransitions();
    State* checkTransitions();
	QB* _qb;
   private:
    long        _lastCall;
    Transition* _transitions[10];
    State*      _endPoints[10];
    int         _numTransitions;
};
#endif
