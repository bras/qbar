#ifndef MotionSensor_h
#define MotionSensor_h

#include "Arduino.h"


class MotionSensor
{
  public:
    MotionSensor();
    void printRawXYZacceleration();
    void update();
    String getOrientations();
    char   getDownside();
    boolean hasMoved();
    int getLastG();
    int getDelta();
    void prepare();
    int getDeltaX();
    int getDeltaY();
    int getDeltaZ();
    int getMaxSecond();
    int getAggregate(int deciSecs);
  private:
    void init();
    void updateA();
    void aggregateAccelleration(int x, int y, int z);
    void resetAggregate();
    String  _orientations;
    char    _downside;
    int     _axPin;
    int     _ayPin;
    int     _azPin;
    int     _rxPin;
    int     _ryPin;
    int     _lastG;
    int     _delta;
    int     _prevX;
    int     _prevY;
    int     _prevZ;
    int     _aggX;
    int     _aggY;
    int     _aggZ;
    char    _aggD[10];
    int     _aggA[10];
    int     _aggI;
    int     _dx;
    int     _dy;
    int     _dz;
    long    _lastUpdate;
    long    _lastAverage;
    long    _lastAdd;
    int     _numAggregates;
    int     _aggregate;
    int     _second[10];
    boolean _moved;
};
#endif
