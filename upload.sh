#!/bin/bash

#Usage: sh upload.sh

#and your arduino must listen to the resetarguments

#Note: Flooding the serial port with debugdata data might break this.
#Note2: that when using the chipantenna xbee, not a large radius was achieved, and be mindful of your direction
#Note3: These settings depend on how your mac interprets the different usb devices.

# At the museum
#	"40764765",	//NR: 01 - ATCOMMAND="ATDL40764765,WR,CN\r"
#	"407692E7",	//NR: 02 - ATCOMMAND="ATDL407692E7,WR,CN\r"
#	"407695AB", //NR: 03 - ATCOMMAND="ATDL407695AB,WR,CN\r"
#	"407691FE",	//NR: 04 - ATCOMMAND="ATDL407691FE,WR,CN\r"
#	"407A7E05",	//NR: 05 - ATCOMMAND="ATDL407A7E05,WR,CN\r"
#	"407692E5",	//NR: 06 - ATCOMMAND="ATDL407692E5,WR,CN\r"

# Current 12

#Be sure to add the zeroes i.e 001 instead of 1 (because of the interpreting in the other end)


SXBEE="009"
NRXBEE=`echo $SXBEE|sed 's/^0*//'`

XX[0]="00000000"
XX[1]="4076970E"
XX[2]="40764774"
XX[3]="407A3938"
XX[4]="407A7DFF"
XX[5]="40764776"
XX[6]="407A3921"
XX[7]="407A38F8"
XX[8]="4076475D"
XX[9]="407695AA"
XX[10]="407696E1"
XX[11]="4076949C"
XX[12]="407A7DFB"

#How it works:
# 1. Specify the 1939 modules target SL (serial low)
# 2. Send join resetnetwork from the 1337 Reset dongle to an ID
# 3. Wait for the xbee to join the resetnetwork
# 4. Send resetcommand over 1939 network
# 5. Upload hex
# 6. ???
# 7. Profit

#point to your uploaders, set the respective arguments and point to a hex file. THis is set up for Arduino Uno, also the Hex files are for arduino uno
DEVICE1939="/dev/cu.usbserial-A900fra9"
DEVICE1337="/dev/cu.usbserial-A5005AK9"
AVRDUDE="/Applications/Arduino.app/Contents/Resources/Java/hardware/tools/avr/bin/avrdude"
AVRCONFIGFILE="-C/Applications/Arduino.app/Contents/Resources/Java/hardware/tools/avr/etc/avrdude.conf"
AVRARGUMENTS="-v -patmega328p -cstk500v1 -P/dev/tty.usbserial-A900fra9 -b115200 -D -Uflash:w:"
MOARAVRARGUMENTS=
HEXFILE="qbar.cpp.hex"
DEVICEQB="usbserial-A50056T6"
DEVICERESET="usbserial-A900fra9"
#HEXFILE="resettest.cpp.hex"

#The command to point the 1939 coordinator to the right xbee
ATCOMMAND="ATDL${XX[NRXBEE]},WR,CN\r"

echo "Configuring 1939 coordinator for its endpoint"
#sleep 1.1 
./arduino-serial -b 115200 -p $DEVICE1939 -s "+++"
#sleep for at command
sleep 1.1 
echo "\033[1;31m$ATCOMMAND\033[m"
./arduino-serial -b 115200 -p $DEVICE1939 -s $ATCOMMAND
sleep 1.1 
echo "1939 configured sucessfully"
#Ready for reset command

echo "Making Cube number ${NRXBEE} join resetnetwork"
NETADJUST=100
NETXBEE=$(($NRXBEE+$NETADJUST))
NETWORKJOINCOMMAND="_n${NETXBEE}"

echo "\033[1;31m$NETWORKJOINCOMMAND\033[m"

./arduino-serial -b 115200 -p $DEVICE1337 -s $NETWORKJOINCOMMAND
echo "Waiting for cube to join network"
sleep 15

echo "Sending reset command"
RESETCOMMAND="_r${SXBEE}"
echo "\033[1;31m$RESETCOMMAND\033[m"
#send the resetcommand over 1939 network
./arduino-serial -b 115200 -p $DEVICE1939 -s $RESETCOMMAND
TIMETORESET="3.5"
#hits with 4, 3.2, 3.7
echo "Command sent, resetting in ${TIMETORESET}"
#./arduino-serial -b 115200 -p $DEVICE1939 -s get -r

#Between 3.2 and 4
sleep $TIMETORESET  
echo "Starting the upload"
#send data over 1939 device
$AVRDUDE $AVRCONFIGFILE $AVRARGUMENTS$HEXFILE
