#ifndef printer
#define printer
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }
#endif

#ifndef Speaker_h
#define Speaker_h

#include "Arduino.h"


//Audio files can only be referenced with their respective number
//THe somo 14d handles up to 512 files on a 2gb sd card
//Remember the files must be named 0001.ad4, not 001.ad4
// 000 - Cat
// 001 - Heartbeat
// 002 - Applause
// 003 - Electric
// 004 - Sacral
// 005 - WHisper
// 006 - Church
// 007 - Ships bell
// 008 - Dripping
// 009 - ?
// 010 - Snes
// 011 - Whistle
// 012 - Sheep
// 013 - Alien
// 014 - Robot lazer
// 015 - Bleep
// 016 - --
// 017 - Moar lazer
// 018 - Game over robot
// 019 - Hello
// 020 - Machine
// 021 - Error buzzer
// 022 - JAzz
// 023 - Sound rythm
// 024 - Metal beatifically
// 025 - Wind
// 026 - FLYBYS
// 027 - Crow
// 028 - harri robot
// 029 - -Whisper always
// 030 - Ownl
// 031 - Thunder
// 032 - Bleep
// 033 - Wind
// 034 - Error buzzer
// 035 - JAzz
// 036 - Sound rythm
// 037 - Metal beatifically
// 038 - Wind
// 039 - FLYBYS
// 040 - Submarine sonar
// 041 - harri robot
// 042 - -Whisper always
// 043 - Ownl
// 044 - Thunder
// 045 - Bleep
// 046 - Wind
// 047 - Error buzzer
// 048 - JAzz
// 049 - Sound rythm
// 050 - Metal beatifically
// 051 - Wind
// 052 - FLYBYS
// 053 - Crow
// 054 - harri robot
// 055 - -Whisper always
// 056 - Ownl

class Speaker
{
  public:
    Speaker();
    
    void  prepare();
    void  reset();
    void  playAudioFile(unsigned int i);
    void  stopPlayBack();
    boolean  isBusy();
  private:
  	void sendProperCommand(unsigned int i);
    void sendCommand(int i);
    unsigned int _minVol; //= 0xfff0;    // Minimum volume
    unsigned int _maxVol; //= 0xfff7;    // Max Volume   
    unsigned int _PlyPse; // = 0xfffe;   // Play or Pause
    unsigned int _Stop; //= 0xFFFF;      // Stop
    int _pinClock; //= 13;
    int _pinData; // =12;
    int _pinBusy; // =7;
    int _pinReset; //=8;
    unsigned int _volLevel; //=0x0005;
    int _Song;                             // current Song
  	unsigned int _vol;                     // current volume

        
  
};
#endif
