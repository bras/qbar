#ifndef interfaces_h
#define interfaces_h

#include "Arduino.h"

class Agitatable
{
  public:
    inline virtual boolean isAgitated() { return false; };
};

class Countable
{
  public:
    inline virtual int getCount() { return 0; };
};


class Timeable
{
  public:
    inline virtual long getTime() { return 0; };
};

#endif