#ifndef printer
#define printer
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }
#endif

#ifndef Microphone_h
#define Microphone_h

#include "Arduino.h"
#include "MotionSensor.h"

class Microphone
{
  public:
   Microphone();
   Microphone(MotionSensor* mSensor);
   void    prepare();
   void    printRawData();
   void    update();
   int     getLastAmp();
   boolean checkClaps(int pattern[]);
   int     getNumClaps();
   int     getTimeSinceLastClap();
  private:
   void resetClaps();
   int           _microphonePin;
   MotionSensor* _mSensor;
   int           _lastAmp;
   int           _claps[10];
   long          _lastClap;
   int           _numClaps;
   int           _deciMax;
   long          _lastAverage;
   int           _numAvg;
   int           _ampAvg;
   boolean       _clapStarted;
   long          _clapStartTime;
 };
#endif
