
#ifndef Color_h
#define Color_h

#include "Arduino.h"

class Color
{
  public:
    Color();
    Color(int h, float s, float v); 
    Color(int r, int g, int b);
    void   setHue(int h);
    void   setSaturation(float h);
    void   setValue(float v);
    int    getHue();
    float  getSaturation();
    float  getValue();
    void   getRgb(byte rgb[]);
  private:
    void rgbToHsv(int r, int g, int b);
    void hsvToRgb(int h, float s, float v, byte rgb[]);
    int _h;
    float _s, _v;
};
#endif

