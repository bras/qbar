#include "Arduino.h"
#include "State.h"

State::State(QB* qb)
{
  _qb = qb;
  _numTransitions = 0;
}


void State::addTransition(Transition* t, State* to)
{
  _transitions[_numTransitions] = t;
  _endPoints[_numTransitions] = to;
  _numTransitions++;
}

void State::initTransitions() {
	for(int i=0; i<_numTransitions; i++) {
  	_transitions[i]->init();
  }
}

State* State::checkTransitions()
{
  State* next = this;
  for(int i=0; i<_numTransitions; i++) {
    if (_transitions[i]->evaluate()) { 
      next = _endPoints[i]; 
      exit();
      next->enter();
      break;
    }
  }
  return next;
}
