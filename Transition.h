#ifndef Transition_h
#define Transition_h

#include "Arduino.h"
#include "State.h"

class State;

class Transition
{
  public:
    Transition();
    inline virtual boolean evaluate() {};
    inline virtual void    init() {};
};

#endif

