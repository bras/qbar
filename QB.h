#ifndef QB_h
#define QB_h

#include "Arduino.h"
#include "Light.h"
#include "Color.h"
#include "MotionSensor.h"
#include "Speaker.h"
#include "Vibrator.h"
#include "Xbee.h"
#include "Microphone.h"


class QB : public NetworkListener {
public:
	QB(); 
	void setColor(Color* c);
	void setColor(Color* c, char side);
	void setColor(Color* c, char sides[], int num); // Må sende inn størrelsen på arrayen med sider...
	void setValue(int b);
	void setValue(int b, char side);
	void setHue(int b);
	void setHue(int b, char side);
	void setSaturation(int b);
	void setSaturation(int b, char side);
	void setOn(boolean on);
	void vibrate(int strength);
	boolean isOn();
	MotionSensor* getMotionSensor();
	Microphone* getMicrophone();
	Light* getLight(char side);
	void prepare();
	void playAudioFile(int filenumber);
	boolean isPlayingAudio();
	void stopPlaying();
	void testForFitness();
	void updateSensors();
	void broadcastState(int state, int value);

	String getXbeeAdress();
	void prepareAndSetupXbee();
	void setQbId(char id);
	char getQbId();
	void broadcastCommand(char id, char data_1, char data_2);
	void broadcastCommand(char id, int data);
	void stateChanged(char qbId, char stateId, int value);
	void commandReceived(int command);
	void enteredState(char stateId, int value);
	boolean pendingCommandIs(int command);
	void setScript(int script);
	int  getScript();
	int  getStateId();
	void resetBlinkM();

private:
	Light _lightE;
	Light _lightS;
	Light _lightW;
	Light _lightN;
	Light _lightD;
	Light _lightU;
	Xbee _xbee;
	MotionSensor _mSensor;
	boolean _on;
	Speaker _speaker;
	Vibrator _vibrator;
	Microphone _microphone;
	int _pendingCommand;
	int _script;
};
#endif
