#ifndef printer
#define printer
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }
#endif

#ifndef Light_h
#define Light_h



#include "Arduino.h"
#include "Color.h"

class Light
{
  public:
    Light();
    Light(int adress); 
    void    setColor(Color* c);
    void    setIntensity(int intensity);
    void    setHue(int hue);
    void    setSaturation(int saturation);
    void    setOn(boolean on);
    boolean isOn();
  private:
    void  setLight(Color* c);
    int   _lightAdress;
    int   _on;
    Color* _color;
    Color* _black;
};
#endif
