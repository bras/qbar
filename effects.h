#ifndef effects
#define effects

#include "Arduino.h"
#include "Effect.h"
#include "Light.h"
#include "QB.h"

/**
 * Blinkeffect
 */
 
class BlinkEffect : public Effect
{
  public: 
    BlinkEffect();
    BlinkEffect(QB* qb);
    BlinkEffect(QB* qb, int time);
    void startEffect();
    void updateEffect();
    void stopEffect();
    void reset();
    int  getLastBlink();
  private:
    void init(QB* qb, int time);
    QB*  _qb;
    int _time;
    unsigned long _lastBlink;
    boolean _on;
};

/**
* Fadeto color effect
Takes in a color and fades to another color over a given amount of time
*/

class FadeCurlEffect : public Effect
{
public:
	FadeCurlEffect();
	FadeCurlEffect(QB* qb);
	FadeCurlEffect(QB* qb, int time, int endHue, int endBrightness);
	
private:
	void init(QB* qb, int time, int endHue, int endBrightness);
	QB* _qb;
	int _time;
	int endHue;
};


/**
 * Pulsateeffect
 */

class PulsateEffect : public Effect
{
  public:
    PulsateEffect();
    PulsateEffect(QB* qb);
    PulsateEffect(QB* qb, int from, int to);
    PulsateEffect(QB* qb, int from, int to, int cycle);
    void startEffect();
    void updateEffect();
    void stopEffect();
    void reset();
  private:
    void init(QB* qb, int from, int to, int cycle);
    QB*   _qb;
    int   _brightness;
    long  _lastChange;
    int   _change;
    int   _from;
    int   _to;
    int   _cycle;
};

/**
 * Blinkeffect
 */
 
class TripEffect : public Effect
{
  public: 
    TripEffect();
    TripEffect(QB* qb);
    void startEffect();
    void updateEffect();
    void stopEffect();
  private:
    void init(QB* qb);
    QB*  _qb;
};

/**
 * Pulsateeffect
 */

//Mykje likt i pulsate effektenz0r
class VibrateEffect : public PulsateEffect
{
  public:
  	VibrateEffect();
    void updateEffect();
    void stopEffect();
    void init();
  private:
    int   _intensity;
};



#endif

