#include "Arduino.h"
#include "Light.h"
#include <Wire.h>

Light::Light() 
{
}

Light::Light(int lightAdress)
{
  _lightAdress = lightAdress;
    
  _black = &Color(0, 0, 0);
  _color = &Color(255, 255, 255);
  
  _on = true;
}


void Light::setColor(Color* c)
{
  _color = c;
  
  if (_on) { setLight(_color); }
}

void Light::setIntensity(int intensity)
{
  _color->setValue(((float) intensity) / 255.0f);
  
  if (_on) { setLight(_color); }
  else           { setLight(_black); }
}

void Light::setHue(int hue) 
{
	_color->setHue(hue);
  
  if (_on) { setLight(_color); }
  else           { setLight(_black); }
}

void Light::setSaturation(int saturation) 
{
	_color->setSaturation(((float) saturation) / 255.0f);
  
  if (_on) { setLight(_color); }
  else           { setLight(_black); }
}

void Light::setOn(boolean on) 
{
  _on = on;
  
  if (_on) { setLight(_color); }
  else           { setLight(_black); }
}

boolean Light::isOn() 
{
  return _on;
}

void Light::setLight(Color* c) 
{
  byte r, g, b;
  
  byte rgb[3];
  c->getRgb(rgb);
  r = rgb[0];
  g = rgb[1];
  b = rgb[2]; 
  
  Wire.beginTransmission(_lightAdress);// join I2C, talk to BlinkM
  Wire.write('n');           // 'n' == set light instantlyz0r
  Wire.write((byte)r);      // value for red channel
  Wire.write((byte)g);      // value for blue channel
  Wire.write((byte)b);      // value for green channel
  Wire.endTransmission();   // leave I2C bus
}





      
