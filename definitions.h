

#ifndef defenitions_h
#define defenitions_h


//Kubetype 0 = loddetkort, 1 = kinakort, 2 = minikube? Dette blir brukt i til dømes av motionsensoren
#define KUBETYPE 1 

//For å raskt slå av og på xbee for debugging
#define USEXBEE

//For å slå av eller på irriterande ljodar
#define USESOUND

//Fuckin pins
//For the resettransistor
#define RESETTRANSISTORPIN_DIGITAL_OUT_PIN 11 

//For the SOMO14D
#define SOMOCLOCK_DIGITAL_OUT_PIN 12 
#define SOMODATA_DIGITAL_OUT_PIN 8 
#define SOMOBUSY_DIGITAL_IN_PIN 7 
#define SOMORESET_DIGITAL_OUT_PIN 4

//For the Accelerometer (analog ins)
#define ADXL335_AX_ANALOG_IN_PIN 0 
#define ADXL335_AY_ANALOG_IN_PIN 1  
#define ADXL335_AZ_ANALOG_IN_PIN 2

//For the vibrator
#define VIBRATOR_PWM_PIN 3

//For the microphone (analog in)
#define MICROPHONE_ANALOG_IN_PIN 3 //4,5 ar reserved for the blinkm

// Konstanter og grenseverdier
#define CLAP_THRESHOLD 160
#define CLAP_MIN_DELAY 150
#define CLAP_MAX_SILENCE 1000
#define CLAP_MIN_DURATION 25
#define CLAP_MAX_DURATION 50
#define CLAP_ACCURACY 25

#define XX0 "00000000"
#define SERIAL_WAIT 2

//For the room
#define NUMBEROFBOXES 7 //six real boxes and one debugbox

#define SLEEPSTATE 1
#define CLAPSTATE 2
#define CHAOTICSTATE 3

// For lights
#define LIGHT_NORTH 6
#define LIGHT_SOUTH 1
#define LIGHT_UP 4
#define LIGHT_DOWN 3
#define LIGHT_WEST 5
#define LIGHT_EAST 2

#define SWVERSION 4

// For states
#define SMACK_DELAY 250
#define SMACK_POWER 45

#define PLAY_REDUCTION_TIME 5
#define PLAY_DELTA_TRESHOLD 10
#define PLAY_INCREASE 8
#define PLAY_AGITATION_TRESHOLD 180 //220 på første batch av kuber
#define PLAY_AGITATION_TIME 5000
#define PLAY_AGITATION_MIN 10
#define PLAY_LIGHT_SWITCH_TIME 50

#define SLEEP_BASE_TIME 1500
#define SLEEP_RANDOM_TIME 500
#define SLEEP_START_TIME 10000 //Denne må endres i live
#define SLEEP_HUE 320

#define SCREAM_AGITATION_LEVEL 250
#define SCREAM_INTENSITY_REDUCTION 5
#define SCREAM_AGITATION_REDUCTION 1

#define PUZZLE_COLOR_1 220
#define PUZZLE_COLOR_2 40

#define SOUND_MIN_WAIT 2000
#define SOUND_MAX_WAIT 20000

// Commands
#define COMMAND_PUZZLE 10
#define COMMAND_SLEEP 66
#define COMMAND_IDENTIFY 99
#define COMMAND_SCRIPT_SENSE 21
#define COMMAND_SCRIPT_CREATE 22
#define COMMAND_SCRIPT_PUZZLE 23
#define COMMAND_STATE_PLAY 24
#define COMMAND_SOUND_PLAYED 30 // Special command that tells the other cubes that a sound has been played
#define COMMAND_SOUND_SHEEP 31
#define COMMAND_SOUND_OWL 32
#define COMMAND_SOUND_CROW 33
#define COMMAND_SOUND_CAT 34
#define COMMAND_SOUND_HELLO 35
#define COMMAND_AGITATED 40

// Scripts
#define SCRIPT_SENSE 1
#define SCRIPT_CREATE 2
#define SCRIPT_PUZZLE 3
#endif
