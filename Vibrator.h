#ifndef printer
#define printer
template<class T> inline Print &operator <<(Print &obj, T arg) { obj.print(arg); return obj; }
#endif

#ifndef Vibrator_h
#define Vibrator_h

#include "Arduino.h"


class Vibrator
{
  public:
   Vibrator();
   void vibrate(int intensity);
   void prepare();
   private:
   int _vibrationPin;
 };
#endif
